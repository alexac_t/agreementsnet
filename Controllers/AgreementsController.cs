﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevinmotionPrueba.Data;
using DevinmotionPrueba.Models;
using Microsoft.AspNetCore.Mvc;

namespace DevinmotionPrueba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgreementsController : ControllerBase
    {
        private readonly AgreementsRepository _repository;

        public AgreementsController(AgreementsRepository repository)
        {
            this._repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable>> Get()
        {
            return await _repository.GetAll();
        }
    }
}
