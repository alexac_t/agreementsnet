﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevinmotionPrueba.Models
{
    public class AgreementsModel
    {
        public String Id{get; set;}
        public String Name{get; set;}
        public String Description{get; set;}
        public int Amount{get; set;}
        public String State{get; set;}
        public String Location{get; set;}

    }
}
