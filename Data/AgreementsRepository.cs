﻿using DevinmotionPrueba.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DevinmotionPrueba.Data
{
    public class AgreementsRepository
    {
        private readonly string _connectionString;
        public ValuesRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("ConnectionSQLServer");
        }

        public Task<List> GetAll()
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("GetAllValues", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var response = new List();
                    await sql.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            response.Add(MapToValue(reader));
                        }
                    }

                    return response;
                }
            }
        }
        private Value MapToValue(SqlDataReader reader)
        {
            return new Value()
            {
                Id = reader["Id"].ToString(),
                Name = reader["Name"].ToString(),
                Description = reader["Description"].ToString(),
                Amount = (int)reader["Amount"],
                State = reader["State"].ToString(),
                Location = reader["Location"].ToString()
            };
        }

    }
}
